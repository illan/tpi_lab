# -*- coding: utf-8 -*-
# Программа рассчета данных по второй части лабораторных ТПИ
# ver 0.3

from tpi_lab_inc import *

# Первая лабораторная, Хэммингово расстояние
def lab1_2():
	corrCodesList = ['' for x in xrange(8)]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab1_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(4):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',corrCodeList[y][0],'+',corrCodeList[y][1],'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')

# Вторая лабораторная, Хэммингово расстояние
def lab2_2():
	corrCodesList = ['' for x in xrange(8)]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab2_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(6):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',corrCodeList[y][0],'+',corrCodeList[y][1],'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')

# Третья лабораторная, Хэммингово расстояние
def lab3_2():
	corrCodesList = ['' for x in xrange(8)]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab3_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(6):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',corrCodeList[y][0],'+',corrCodeList[y][1],'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')

# Четвертая лабораторная, Хэммингово расстояние
def lab4_2():
	corrCodesList = ['' for x in xrange(8 )]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab4_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(6):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',''.join(corrCodeList[y][0]),'+',''.join(corrCodeList[y][1]),'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')

# Пятая лабораторная, Хэммингово расстояние
def lab5_2():
	i, j = 0, 0
	k, corrCode = lab5_CorrCode()
	k = [['','']for x in xrange(45)]
	summCode = ['' for x in xrange(45)]
	for y in xrange(45):
		if i < 9 and j < 9: i += 1
		elif i == 9: j += 1; i = i-8+j
		if j < i:
			k[y][0] = corrCode[j]
			k[y][1] = corrCode[i]
		for r in xrange(5):
			L = [('0' if k[y][0][r] == k[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y,'\t',''.join(k[y][0]),'+', ''.join(k[y][1]),'=',summCode[y],'\tHemSum = ', list(summCode[y]).count('1')

# Шестая лабораторная, Хэммингово расстояние
def lab6_2():
	corrCodesList = ['' for x in xrange(16 )]
	corrCodeList  = [[] for x in xrange(120)]
	summCode = 		['' for x in xrange(120)]
	for x in xrange(16):
		a, corrCodesList[x] = lab6_CorrCode(x)
	i, j = 0, 0
	for y in xrange(120):	
		if i < 15 and j < 15: i += 1
		elif i == 15: j += 1; i = i-14+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(7):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t\t',''.join(corrCodeList[y][0]),'+',''.join(corrCodeList[y][1]),'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')

# Седьмая лабораторная, Хэммингово расстояние
def lab7_2():
	corrCodesList = ['' for x in xrange(16 )]
	corrCodeList  = [[] for x in xrange(120)]
	summCode = 		['' for x in xrange(120)]
	for x in xrange(16):
		a, corrCodesList[x] = lab7_CorrCode(x)
	i, j = 0, 0
	for y in xrange(120):	
		if i < 15 and j < 15: i += 1
		elif i == 15: j += 1; i = i-14+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(7):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t\t',''.join(corrCodeList[y][0]),'+',''.join(corrCodeList[y][1]),'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')

