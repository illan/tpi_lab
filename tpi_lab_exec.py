# -*- coding: utf-8 -*-
# Исполняющая программа по лабораторным ТПИ
# ver 0.3

from tpi_lab_inc import *
from tpi_lab_1 import *
from tpi_lab_2 import *

labType = raw_input("Тип лабораторной:")
labNum  = raw_input("Номер лабораторной:")

def switch_case_0(case):
	return {
	'1' : '1',
	'2' : '2',
	}.get(case, "an out of range number")

def switch_case_1(case):
	return {
	'1' : "lab1_",
	'2' : "lab2_",
	'3' : "lab3_",
	'4' : "lab4_",
	'5' : "lab5_",
	'6' : "lab6_",
	'7' : "lab7_",
	}.get(case, "an out of range number")

eval(switch_case_1(labNum)+switch_case_0(labType)+'()')			