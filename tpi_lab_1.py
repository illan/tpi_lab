# -*- coding: utf-8 -*-
# Программа рассчета данных по первой части лабораторных ТПИ
# ver 0.3

from tpi_lab_inc import *

# Первая лабораторная, корректирующий код
def lab1_1():
	lab1CorrCode = []
	num = 0
	print '#\t\tIn\t\tOut'
	while num < 8:
		primaryCode, corrCode = lab1_CorrCode(num)
		print num,'\t\t',primaryCode,'\t\t',corrCode
		num += 1

# Вторая лабораторная, корректирующий код
def lab2_1():
	num = 0
	print '#\t\tIn\t\tOut'
	while num < 8:
		primaryCode, corrCode = lab2_CorrCode(num)					#Имхо, очевидно
		print num + 1,'\t\t',primaryCode,'\t\t',corrCode
		num += 1

# Третья лабораторная, корректирующий код
def lab3_1():
	num = 0
	print "#\t\tIn\t\tOut"
	while num < 8:
		primaryCode, corrCode = lab3_CorrCode(num)
		print num,'\t\t',primaryCode,'\t\t',corrCode
		num += 1

# Четвертая лабораторная, корректирующий код
def lab4_1():
	num = 0
	print "#\t\tIn\t\tOut"
	while num < 8:
		primaryCode, corrCode = lab4_CorrCode(num)
		print num,'\t\t',primaryCode,'\t\t',''.join(corrCode)
		num += 1

# Пятая лабораторная, корректирующий код
def lab5_1():
	k, corrCode = lab5_CorrCode()
	for x in xrange(10):
		print int(k[x][0])+1,int(k[x][1])+1,'=', ''.join(corrCode[x])

# Шестая лабораторная, корректирующий код
def lab6_1():
	num = 0
	print "#\t\tIn\t\tOut"
	while num < 16:
		primaryCode, corrCode = lab6_CorrCode(num)
		print num,'\t\t',primaryCode,'\t\t',''.join(corrCode)
		num += 1

# Седьмая лабораторная, корректирующий код
def lab7_1():
	num = 0
	print "#\t\tIn\t\tOut"
	while num < 16:
		primaryCode, corrCode = lab7_CorrCode(num)
		print num,'\t\t',primaryCode,'\t\t',''.join(corrCode)
		num += 1
