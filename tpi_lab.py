# -*- coding: utf-8 -*-
# Программа рассчета данных по лабораторным ТПИ
# ver 0.3

def refact_bin_1to5(primaryCode):
	if len(primaryCode) == 3: primaryCode = primaryCode.replace('0b','00')		#Выпиливаем отображение 
	if len(primaryCode) == 4: primaryCode = primaryCode.replace('0b','0')		#бинарников и приводим 
	if len(primaryCode) == 5: primaryCode = primaryCode.replace('0b','')		#к удобному виду
	return primaryCode

def refact_bin_6to7(primaryCode):
	if len(primaryCode) == 3: primaryCode = primaryCode.replace('0b','000')		#Выпиливаем отображение 
	if len(primaryCode) == 4: primaryCode = primaryCode.replace('0b','00')		#бинарников и приводим 
	if len(primaryCode) == 5: primaryCode = primaryCode.replace('0b','0')		#к удобному виду
	if len(primaryCode) == 6: primaryCode = primaryCode.replace('0b','')
	return primaryCode

#############################
## Лабораторная работа №1 ###
#############################

# Корректирующий код лабы №1
def lab1_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_1to5(primaryCode)
	if list(primaryCode).count('1')%2 == 0: corrCode = primaryCode + '0'		 
	else: corrCode = primaryCode + '1'		
	num += 1
	return primaryCode, corrCode												

# Первая лабораторная, корректирующий код
def lab1_1():
	lab1CorrCode = []
	num = 0
	print '#\t\t\tIn\t\t\tOut'
	while num < 8:
		primaryCode, corrCode = lab1_CorrCode(num)
		print num,'\t\t\t',primaryCode,'\t\t',corrCode
		num += 1
#lab1_1()

# Первая лабораторная, Хэммингово расстояние
def lab1_2():
	corrCodesList = ['' for x in xrange(8)]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab1_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(4):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',corrCodeList[y][0],'+',corrCodeList[y][1],'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')
#lab1_2()

#############################
## Лабораторная работа №2 ###
#############################

# Корректирующий код лабы №2
def lab2_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_1to5(primaryCode)
	corrCode = primaryCode + primaryCode
	return primaryCode, corrCode

# Вторая лабораторная, корректирующий код
def lab2_1():
	num = 0
	print '#\t\t\tIn\t\t\tOut'
	while num < 8:
		primaryCode, corrCode = lab2_CorrCode(num)					#Имхо, очевидно
		print num + 1,'\t\t\t',primaryCode,'\t\t',corrCode
		num += 1
#lab2_1()

# Вторая лабораторная, Хэммингово расстояние
def lab2_2():
	corrCodesList = ['' for x in xrange(8)]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab2_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(6):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',corrCodeList[y][0],'+',corrCodeList[y][1],'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')
#lab2_2()

#############################
## Лабораторная работа №3 ###
#############################

# Корректирующий код лабы №3
def lab3_CorrCode(num):
	primaryCode = str(bin(num))
	corrCodeInv = ''
	primaryCode = refact_bin_1to5(primaryCode)
	for x in xrange(3):
		if primaryCode[x] == '0':   corrCodeInv += primaryCode[x].replace('0','1')	#Считаем для нечетного кол-ва
		else: corrCodeInv += primaryCode[x].replace('1','0')	#
	if list(primaryCode).count('1')%2 == 0: corrCode = primaryCode + primaryCode	#Считаем корректирующий код
	else: corrCode = primaryCode + corrCodeInv	#
	return primaryCode, corrCode

# Третья лабораторная, корректирующий код
def lab3_1():
	num = 0
	print "#\t\t\tIn\t\t\tOut"
	while num < 8:
		primaryCode, corrCode = lab3_CorrCode(num)
		print num,'\t\t\t',primaryCode,'\t\t',corrCode
		num += 1
#lab3_1()

# Третья лабораторная, Хэммингово расстояние
def lab3_2():
	corrCodesList = ['' for x in xrange(8)]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab3_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(6):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',corrCodeList[y][0],'+',corrCodeList[y][1],'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')
#lab3_2()

#############################
## Лабораторная работа №4 ###
#############################

# Корректирующий код лабы №4
def lab4_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_1to5(primaryCode)
	corrCode = ['','','','','','']
	for x in xrange(0,5,2):
		#print x, x/2 , primaryCode[x/2], primaryCode
		if primaryCode[x/2] == '0':   corrCode[x] = primaryCode[x/2]; corrCode[x+1] = '1'
		else: corrCode[x] = primaryCode[x/2]; corrCode[x+1] = '0'
	return primaryCode, corrCode

# Четвертая лабораторная, корректирующий код
def lab4_1():
	num = 0
	print "#\t\t\tIn\t\t\tOut"
	while num < 8:
		primaryCode, corrCode = lab4_CorrCode(num)
		print num,'\t\t\t',primaryCode,'\t\t',''.join(corrCode)
		num += 1
#lab4_1()

# Четвертая лабораторная, Хэммингово расстояние
def lab4_2():
	corrCodesList = ['' for x in xrange(8 )]
	corrCodeList  = [[] for x in xrange(28)]
	summCode = 		['' for x in xrange(28)]
	for x in xrange(8):
		a, corrCodesList[x] = lab4_CorrCode(x)
	i, j = 0, 0
	for y in xrange(28):	
		if i < 7 and j < 7: i += 1
		elif i == 7: j += 1; i = i-6+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(6):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t',''.join(corrCodeList[y][0]),'+',''.join(corrCodeList[y][1]),'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')
#lab4_2()

#############################
## Лабораторная работа №5 ###
#############################

# Корректирующий код лабы №5
def lab5_CorrCode():
	i, j = 0, 0
	k = [['',''] for x in xrange(10)]
	corrCode = [[] for x in xrange(10)]
	corrCodeInp = '00000'
	for x in xrange(10):
		if i<4 and j<4: i += 1
		elif i == 4: j += 1; i = i-3+j
		if j < i:
			k[x][0] = j
			k[x][1] = i
		corrCode[x] = list('1' if y == k[x][0] or y == k[x][1] else '0' for y in xrange(5))[::-1]
	return k,corrCode

# Пятая лабораторная, корректирующий код
def lab5_1():
	k, corrCode = lab5_CorrCode()
	for x in xrange(10):
		print int(k[x][0])+1,int(k[x][1])+1,'=', ''.join(corrCode[x])
#lab5_1()

# Пятая лабораторная, Хэммингово расстояние
def lab5_2():
	i, j = 0, 0
	k, corrCode = lab5_CorrCode()
	k = [['','']for x in xrange(45)]
	summCode = ['' for x in xrange(45)]
	for y in xrange(45):
		if i < 9 and j < 9: i += 1
		elif i == 9: j += 1; i = i-8+j
		if j < i:
			k[y][0] = corrCode[j]
			k[y][1] = corrCode[i]
		for r in xrange(5):
			L = [('0' if k[y][0][r] == k[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print ''.join(k[y][0]),'+', ''.join(k[y][1]),'=',summCode[y],'\tHemSum = ', list(summCode[y]).count('1')
#lab5_2()

#############################
## Лабораторная работа №6 ###
#############################

# Корректирующий код лабы №6
def lab6_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_6to7(primaryCode)
	corrCode = ['','','','','','','']
	primaryCodeList = list(primaryCode)
	if (int(primaryCodeList[0])+int(primaryCodeList[1])+int(primaryCodeList[3]))%2 == 0:   corrCode[0] = '0'
	else: corrCode[0] = '1'

	if (int(primaryCodeList[0])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[1] = '0'
	else: corrCode[1] = '1'

	if (int(primaryCodeList[1])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[3] = '0'
	else: corrCode[3] = '1'

	corrCode[2] += primaryCodeList[0]
	corrCode[4] += primaryCodeList[1]
	corrCode[5] += primaryCodeList[2]
	corrCode[6] += primaryCodeList[3]
	return primaryCode, corrCode

# Шестая лабораторная, корректирующий код
def lab6_1():
	num = 0
	print "#\t\t\tIn\t\t\tOut"
	while num < 16:
		primaryCode, corrCode = lab6_CorrCode(num)
		print num,'\t\t\t',primaryCode,'\t\t',''.join(corrCode)
		num += 1
#lab6_1()

# Шестая лабораторная, Хэммингово расстояние
def lab6_2():
	corrCodesList = ['' for x in xrange(16 )]
	corrCodeList  = [[] for x in xrange(120)]
	summCode = 		['' for x in xrange(120)]
	for x in xrange(16):
		a, corrCodesList[x] = lab6_CorrCode(x)
	i, j = 0, 0
	for y in xrange(120):	
		if i < 15 and j < 15: i += 1
		elif i == 15: j += 1; i = i-14+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(7):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t\t',''.join(corrCodeList[y][0]),'+',''.join(corrCodeList[y][1]),'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')
#lab6_2()

#############################
## Лабораторная работа №7 ###
#############################

# Корректирующий код лабы №7
def lab7_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_6to7(primaryCode)
	corrCode = ['','','','','','','']
	primaryCodeList = list(primaryCode)
	if (int(primaryCodeList[0])+int(primaryCodeList[1])+int(primaryCodeList[3]))%2 == 0:   corrCode[4] = '0'
	elif (int(primaryCodeList[0])+int(primaryCodeList[1])+int(primaryCodeList[3]))%2 != 0: corrCode[4] = '1'

	if (int(primaryCodeList[0])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[5] = '0'
	elif (int(primaryCodeList[0])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 != 0: corrCode[5] = '1'

	if (int(primaryCodeList[1])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[6] = '0'
	elif (int(primaryCodeList[1])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 != 0: corrCode[6] = '1'

	corrCode[0] += primaryCodeList[0]
	corrCode[1] += primaryCodeList[1]
	corrCode[2] += primaryCodeList[2]
	corrCode[3] += primaryCodeList[3]
	return primaryCode, corrCode

# Седьмая лабораторная, корректирующий код
def lab7_1():
	num = 0
	print "#\t\t\tIn\t\t\tOut"
	while num < 16:
		primaryCode, corrCode = lab7_CorrCode(num)
		print num,'\t\t\t',primaryCode,'\t\t',''.join(corrCode)
		num += 1
#lab7_1()

# Седьмая лабораторная, Хэммингово расстояние
def lab7_2():
	corrCodesList = ['' for x in xrange(16 )]
	corrCodeList  = [[] for x in xrange(120)]
	summCode = 		['' for x in xrange(120)]
	for x in xrange(16):
		a, corrCodesList[x] = lab7_CorrCode(x)
	i, j = 0, 0
	for y in xrange(120):	
		if i < 15 and j < 15: i += 1
		elif i == 15: j += 1; i = i-14+j
		if j < i:
			corrCodeList[y].append(corrCodesList[j])
			corrCodeList[y].append(corrCodesList[i])
		for r in xrange(7):	
			L = [('0' if corrCodeList[y][0][r] == corrCodeList[y][1][r] else '1' )]
			summCode[y] += ''.join(L)
		print y+1,'\t\t',''.join(corrCodeList[y][0]),'+',''.join(corrCodeList[y][1]),'=',summCode[y],\
			 '\tHemSum = ', summCode[y].count('1')
#lab7_2()