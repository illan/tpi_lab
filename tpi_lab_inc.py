# -*- coding: utf-8 -*-
# Инклюды доп. данных и предварительных расчетов для основных лаб.
# ver 0.3


#
# Выпиливание бинарников и приводим их к удобному виду
#

def refact_bin_1to5(primaryCode):
	if len(primaryCode) == 3: primaryCode = primaryCode.replace('0b','00')		 
	if len(primaryCode) == 4: primaryCode = primaryCode.replace('0b','0')		 
	if len(primaryCode) == 5: primaryCode = primaryCode.replace('0b','')		
	return primaryCode

def refact_bin_6to7(primaryCode):
	if len(primaryCode) == 3: primaryCode = primaryCode.replace('0b','000')		
	if len(primaryCode) == 4: primaryCode = primaryCode.replace('0b','00')		
	if len(primaryCode) == 5: primaryCode = primaryCode.replace('0b','0')		
	if len(primaryCode) == 6: primaryCode = primaryCode.replace('0b','')
	return primaryCode

#
# Корректирующие коды
#

# Корректирующий код лабы №1
def lab1_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_1to5(primaryCode)
	if list(primaryCode).count('1')%2 == 0: corrCode = primaryCode + '0'		 
	else: corrCode = primaryCode + '1'		
	num += 1
	return primaryCode, corrCode												

# Корректирующий код лабы №2
def lab2_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_1to5(primaryCode)
	corrCode = primaryCode + primaryCode
	return primaryCode, corrCode

# Корректирующий код лабы №3
def lab3_CorrCode(num):
	primaryCode = str(bin(num))
	corrCodeInv = ''
	primaryCode = refact_bin_1to5(primaryCode)
	for x in xrange(3):
		if primaryCode[x] == '0':   corrCodeInv += primaryCode[x].replace('0','1')	#Считаем для нечетного кол-ва
		else: corrCodeInv += primaryCode[x].replace('1','0')	#
	if list(primaryCode).count('1')%2 == 0: corrCode = primaryCode + primaryCode	#Считаем корректирующий код
	else: corrCode = primaryCode + corrCodeInv	#
	return primaryCode, corrCode

# Корректирующий код лабы №4
def lab4_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_1to5(primaryCode)
	corrCode = ['','','','','','']
	for x in xrange(0,5,2):
		#print x, x/2 , primaryCode[x/2], primaryCode
		if primaryCode[x/2] == '0':   corrCode[x] = primaryCode[x/2]; corrCode[x+1] = '1'
		else: corrCode[x] = primaryCode[x/2]; corrCode[x+1] = '0'
	return primaryCode, corrCode

# Корректирующий код лабы №5
def lab5_CorrCode():
	i, j = 0, 0
	k = [['',''] for x in xrange(10)]
	corrCode = [[] for x in xrange(10)]
	corrCodeInp = '00000'
	for x in xrange(10):
		if i<4 and j<4: i += 1
		elif i == 4: j += 1; i = i-3+j
		if j < i:
			k[x][0] = j
			k[x][1] = i
		corrCode[x] = list('1' if y == k[x][0] or y == k[x][1] else '0' for y in xrange(5))[::-1]
	return k,corrCode

# Корректирующий код лабы №6
def lab6_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_6to7(primaryCode)
	corrCode = ['','','','','','','']
	primaryCodeList = list(primaryCode)
	if (int(primaryCodeList[0])+int(primaryCodeList[1])+int(primaryCodeList[3]))%2 == 0:   corrCode[0] = '0'
	else: corrCode[0] = '1'

	if (int(primaryCodeList[0])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[1] = '0'
	else: corrCode[1] = '1'

	if (int(primaryCodeList[1])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[3] = '0'
	else: corrCode[3] = '1'

	corrCode[2] += primaryCodeList[0]
	corrCode[4] += primaryCodeList[1]
	corrCode[5] += primaryCodeList[2]
	corrCode[6] += primaryCodeList[3]
	return primaryCode, corrCode

# Корректирующий код лабы №7
def lab7_CorrCode(num):
	primaryCode = str(bin(num))
	primaryCode = refact_bin_6to7(primaryCode)
	corrCode = ['','','','','','','']
	primaryCodeList = list(primaryCode)
	if (int(primaryCodeList[0])+int(primaryCodeList[1])+int(primaryCodeList[3]))%2 == 0:   corrCode[4] = '0'
	elif (int(primaryCodeList[0])+int(primaryCodeList[1])+int(primaryCodeList[3]))%2 != 0: corrCode[4] = '1'

	if (int(primaryCodeList[0])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[5] = '0'
	elif (int(primaryCodeList[0])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 != 0: corrCode[5] = '1'

	if (int(primaryCodeList[1])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 == 0:   corrCode[6] = '0'
	elif (int(primaryCodeList[1])+int(primaryCodeList[2])+int(primaryCodeList[3]))%2 != 0: corrCode[6] = '1'

	corrCode[0] += primaryCodeList[0]
	corrCode[1] += primaryCodeList[1]
	corrCode[2] += primaryCodeList[2]
	corrCode[3] += primaryCodeList[3]
	return primaryCode, corrCode